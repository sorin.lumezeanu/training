Do modify the variables to match our test environment

```yaml
stages:
- trufflehog
trufflehog:
  stage: trufflehog
  image: defdev/trufflehog
  variables: 
    SOURCE_REPO: https://github.com/dxa4481/truffleHog.git
    DOJO_URL: https://defectdojo.herokuapp.com
    DOJO_ENGAGEMENT_ID: 1
    DOJO_API_KEY: admin:1dfdfa2042567ec751f6b3fa96038b743ea6f1cc
  script:
  - echo "we ran trufflehog scanner here"